#!/bin/bash

# RedHat system
if [ -f /bin/yum ]; then

  # Install base packages and clean up package database
  yum -y install yum-utils
  package-cleanup -y --oldkernels --count=1
  yum clean all

  # Network config
  sed -i '/^(HWADDR|UUID)=/d' /etc/sysconfig/network-scripts/ifcfg-e*

  #Cleanup
  rm -f ~root/anaconda-ks.cfg
  cat /dev/null > /var/log/audit/audit.log

  # Persistent devices
  rm -f /etc/udev/rules.d/70*

fi


# Debian system
if [ -f /usr/bin/apt ]; then

  apt clean
  cloud-init clean --logs
  rm -rf /var/log/unattended-upgrades

fi


# All systems

# Extraneous services
service rsyslog stop
service auditd stop

# Old logs
logrotate -f /etc/logrotate.conf
rm -f /var/log/*-???????? /var/log/*.gz
rm -f /var/log/dmesg.old
rm -rf /var/log/anaconda
cat /dev/null > /var/log/wtmp
cat /dev/null > /var/log/lastlog
cat /dev/null > /var/log/grubby

# Temp files
rm -rf /tmp/*
rm -rf /var/tmp/*

# SSH host keys
rm -f /etc/ssh/*key*

# History
rm -f ~root/.bash_history
unset HISTFILE
rm -rf ~root/.ssh/

# Machine id
rm -f /etc/machine-id

# Build ~root/firstboot.sh
cat << EOF > ~root/firstboot.sh
#!/bin/bash
/bin/systemd-machine-id-setup
EOF
chown root:root ~root/firstboot.sh
chmod 755 ~root/firstboot.sh

# Create firstboot systemd service
cat << EOF > /etc/systemd/system/firstboot.service
[Unit]
Description=First Boot Script
After=network.target
[Service]
ExecStart=/root/firstboot.sh
[Install]
WantedBy=multi-user.target
EOF

# Enable firstboot systemd service
chmod 644 /etc/systemd/system/firstboot.service
systemctl enable firstboot.service

# Clear history and shutdown
history -c

if [ -f /sbin/sys-unconfig ]; then
  /sbin/sys-unconfig
fi